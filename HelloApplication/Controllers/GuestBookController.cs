﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HelloApplication.Context;
using HelloApplication.Models;

namespace HelloApplication.Controllers
{
    public class GuestBookController : Controller
    {
        // GET: GuestBook
        public ActionResult Index(int page = 1)
        {
            var messages = new List<Message>();
            int count;
            using (var db = new MessagesContext())
            {
                /*db.Database.Log = (string message) =>
                {
                    using (var file = System.IO.File.AppendText(@"c:\temp\log.sql"))
                    {
                        file.WriteLine(message);
                    }
                };*/

                messages = db.Messages
                    .OrderByDescending(m => m.Id)
                    .Skip((page - 1) * Constants.PageSize)
                    .Take(Constants.PageSize)
                    .Select(m => new Message
                    {
                        Id = m.Id,
                        Post = m.Post
                    }).ToList();
                count = db.Messages.Count();
            }

            ViewBag.CurrentPage = page;
            ViewBag.Count = count;
            ViewBag.ControllerName = ControllerContext.RouteData.Values["controller"].ToString();

            return View(messages);
        }

        //[Authorize]
        public PartialViewResult Post()
        {            
            return PartialView("Post");
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Post(Message message)
        {
            if (ModelState.IsValid)
            {
                using (var db = new MessagesContext())
                {
                    db.Messages.Add(new Entities.MessageEntity
                    {
                        Post = message.Post
                    });
                    db.SaveChanges();
                }
            }           

            return RedirectToAction("Index");
        }        
    }
}