﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelloApplication.Context;
using HelloApplication.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using HelloApplication.Filters;

namespace HelloApplication.Controllers
{
    [MyUnauthorized(Users = Constants.MainUser)]
    public class UsersController : Controller
    {
        private readonly int _pageSize = int.Parse(ConfigurationManager.AppSettings["UsersPage"]);

        // GET: Users
        public ActionResult Index(int page = 1)
        {
            using (var context = new MessagesContext())
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);

                var users = context.Users.OrderBy(u => u.UserName == Constants.MainUser ? "" : u.UserName)
                    .Skip((page - 1) * _pageSize)
                    .Take(_pageSize).ToList().Select(u => new User
                    {
                        Id = u.Id,
                        IsAdmin = manager.IsInRole(u.Id, Constants.Roles.AdminRole),
                        UserName = u.UserName
                    }).ToList();

                /*var users = new List<User>();
                var n = 65;
                for (var i = 0; i <= n; i++)
                {
                    users.Add(new User
                    {
                        Id = i.ToString(),
                        UserName = $"oleg_{i}",
                    });
                }

                users = users.Skip((page - 1) * _pageSize).Take(_pageSize).ToList();*/
                ViewBag.CurrentPage = page;
                ViewBag.Count = context.Users.Count();
                ViewBag.ControllerName = ControllerContext.RouteData.Values["controller"].ToString();

                return View(users);
            }
        }        
       
        // POST: Users/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void Update(string id, bool isAdmin)
        {
            try
            {
                using (var context = new MessagesContext())
                {
                    var store = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(store);

                    if (isAdmin)
                    {
                        manager.AddToRole(id, Constants.Roles.AdminRole);
                    }
                    else
                    {
                        manager.RemoveFromRole(id, Constants.Roles.AdminRole);
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Произошла некоторая ошибка");
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(string id)
        {
            try
            {
                using (var context = new MessagesContext())
                {
                    var store = new UserStore<ApplicationUser>(context);
                    var manager = new UserManager<ApplicationUser>(store);
                    var user = manager.FindById(id);
                    manager.Delete(user);
                }
            }
            catch (Exception)
            {
                throw new Exception("Произошла некоторая ошибка");
            }
            return RedirectToAction("Index");
        }
    }
}
