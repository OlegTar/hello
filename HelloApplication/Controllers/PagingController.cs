﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using HelloApplication.Models;

namespace HelloApplication.Controllers
{
    public class PagingController : Controller
    {
        [ChildActionOnly]
        public PartialViewResult Paging(int currentPage, int count, string controllerName)
        {
            var countPages = (int)Math.Ceiling((double)count / Constants.PageSize);
            if (currentPage > countPages)
            {
                currentPage = countPages;
            }
            else if (currentPage < 1)
            {
                currentPage = 1;
            }

            var paging = new Paging
            {
                CurrentPage = currentPage,
                LastPage = countPages,
                ControllerName = controllerName
            };


            /*var pages = new List<int>();
            if (currentPage == 1)
            {
                pages.Add(1);
                var minPage = Math.Min(MaxCountLinks, countPages);
                for (var i = 2; i < minPage; i++)
                {
                    pages.Add(i);
                }
            }
            else if (currentPage == countPages)
            {
                var firstPage = Math.Max(1, currentPage - MaxCountLinks);
                for (var i = firstPage; i < currentPage; i++)
                {
                    pages.Add(i);
                }
            }
            else
            {
                pages.Add(currentPage - 1);
                pages.Add(currentPage);
                pages.Add(currentPage + 1);
            }*/

            var pages = new List<int>();
            if (currentPage == 1)
            {
                pages.Add(currentPage);
                var minPage = Math.Min(Constants.MaxCountLinks, countPages);
                for (var i = 2; i <= minPage; i++)
                {
                    pages.Add(i);
                }
            }
            else if (currentPage == countPages)
            {                
                var firstPage = Math.Max(1, currentPage - Constants.MaxCountLinks);
                for (var i = firstPage; i < currentPage; i++)
                {
                    pages.Add(i);
                }
                pages.Add(currentPage);
            }
            else
            {
                var halfCount = (int)Math.Floor((double)Constants.MaxCountLinks / 2);
                var firstLink = Math.Max(1, currentPage - halfCount);
                for (var i = firstLink; i <= currentPage; i++)
                {
                    pages.Add(i);
                }
                var otherHalfCount = Constants.MaxCountLinks - halfCount;
                var lastLink = Math.Min(currentPage + otherHalfCount, countPages);
                for (var i = currentPage + 1; i <= lastLink; i++)
                {
                    pages.Add(i);
                }
            }
            paging.Pages = pages;

            return PartialView(paging);
        }
    }
}