﻿using System;
using System.Web.Mvc;
using HelloApplication.Context;
using HelloApplication.Models;

namespace HelloApplication.Controllers
{
    public class MessageController : Controller
    {
        // GET: Message
        public PartialViewResult Index(Message message)
        {
            return PartialView(message);
        }

        public PartialViewResult Message(int id)
        {
            using (var db = new MessagesContext())
            {
                var messageEntity = db.Messages.Find(id);
                if (messageEntity == null)
                {
                    throw new Exception("сообщение не найдено!");
                }

                var message = new Message
                {
                    Id = messageEntity.Id,
                    Post = messageEntity.Post,
                };

                return PartialView("Message", message);
            }
        }

        public PartialViewResult Edit(int id)
        {
            using (var db = new MessagesContext())
            {
                var messageEntity = db.Messages.Find(id);
                if (messageEntity == null)
                {
                    throw new Exception("сообщение не найдено!");
                }

                var message = new Message
                {
                    Id = messageEntity.Id,
                    Post = messageEntity.Post,
                };

                return PartialView(message);
            }
        }

        [HttpPost]
        public PartialViewResult Edit(Message message)
        {
            using (var db = new MessagesContext())
            {
                var messageEntity = db.Messages.Find(message.Id);
                if (messageEntity == null)
                {
                    throw new Exception("сообщение не найдено!");
                }

                if (ModelState.IsValid)
                {
                    messageEntity.Post = message.Post;
                    db.SaveChanges();
                }

                var newMessage = new Message
                {
                    Id = messageEntity.Id,
                    Post = messageEntity.Post,
                };

                return PartialView("Message", newMessage);
            }
        }

        public ActionResult Delete(int id)
        {
            using (var db = new MessagesContext())
            {
                var message = db.Messages.Find(id);
                if (message != null)
                {
                    db.Messages.Remove(message);
                    db.SaveChanges();
                }
            }
            return RedirectToAction("Index", "GuestBook");
        }
    }
}