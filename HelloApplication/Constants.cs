﻿namespace HelloApplication
{
    public class Constants
    {
        public const int MaxCountLinks = 3;
        public const int PageSize = 10;

        public class Roles
        {
            public const string AdminRole = "adminRole";
        }

        public const string MainUser = "admin@test.ru";
    }
}