namespace HelloApplication.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using HelloApplication.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<HelloApplication.Context.MessagesContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HelloApplication.Context.MessagesContext context)
        {
            if (!context.Roles.Any(r => r.Name == HelloApplication.Constants.Roles.AdminRole))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = HelloApplication.Constants.Roles.AdminRole };

                manager.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == HelloApplication.Constants.MainUser))
            {
                var store = new UserStore<ApplicationUser>(context);
                var manager = new UserManager<ApplicationUser>(store);
                var user = new ApplicationUser { UserName = HelloApplication.Constants.MainUser };

                manager.Create(user, "qwerty1234");
                manager.AddToRole(user.Id, HelloApplication.Constants.Roles.AdminRole);
            }

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
