﻿var message = (function() {
    return {
        submit: function(id) {
            $("#" + id).submit();
            return false;
        },
        deleteMessage: function (node, e) {

            var link = $(node).attr("href");
            $("#dialog-confirm").dialog("option",
                "buttons",
                {
                    "Да": function() {
                        location.href = link;
                        $("#dialog-confirm").dialog("close");
                    },
                    "Отмена": function() {
                        $("#dialog-confirm").dialog("close");
                    }
                }
            );
            $("#dialog-confirm").dialog("open");
            e.preventDefault();
        },
        changeAdminForm: function (input) {
            var $input = $(input);
            $input.attr('disabled', 'disabled');
            var elements = input.form.elements;
            var data = {};
            for (var i = 0; i < elements.length; i++) {
                data[elements[i].name] = elements[i].value;
            }
            data["isAdmin"] = $input.prop('checked');

            $.ajax(input.form.action,
                {
                    method: "POST",
                    data: data
                })
            .fail(function () {
                var checked = $input.prop('checked');
                $input.prop('checked', !checked);
            })
            .complete(function() {
                $input.removeAttr('disabled');
            });
        }
    };
})();

$(function () {
    //$(".delete").click(message.deleteMessage);

    $("#dialog-confirm").dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        autoOpen: false
    });
});