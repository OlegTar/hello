﻿using System.ComponentModel.DataAnnotations;

namespace HelloApplication.Models
{
    public class Message
    {
        public int Id { get; set; }
        [DataType(DataType.MultilineText)]
        [Required(ErrorMessage = "Введите текст")]
        public string Post { get; set; }
    }
}