﻿using System.ComponentModel.DataAnnotations;

namespace HelloApplication.Models
{
    public class User
    {
        public string Id { get; set; }
        public string UserName { get; set; }
        [UIHint("AdminRoleEdit")]
        public bool IsAdmin { get; set; }
    }
}