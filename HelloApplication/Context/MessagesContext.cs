﻿using System.Data.Entity;
using HelloApplication.Entities;
using HelloApplication.Models;

namespace HelloApplication.Context
{
    public class MessagesContext: ApplicationDbContext
    {
        public MessagesContext()
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MessagesContext, Migrations.Configuration>("messages"));
            Database.SetInitializer<MessagesContext>(new MessageInitializer());
        }

        public DbSet<MessageEntity> Messages { get; set; }
    }
}