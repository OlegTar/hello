﻿using System.ComponentModel.DataAnnotations.Schema;

namespace HelloApplication.Entities
{
    [Table("Messages")]
    public class MessageEntity
    {
        public int Id { get; set; }
        public string Post { get; set; }       
    }
}